#!/bin/bash

for e in $(tr "\000" "\n" < /proc/1/environ); do
        eval "export $e"
        echo "$e"  >> /var/lib/mysql/env.txt
done

getbytes() {
  for v in "${@:-$(</dev/stdin)}"
  do
    echo $v | awk \
      'BEGIN{IGNORECASE = 1}
       function printpower(n,b,p) {printf "%u\n", n*b^p; next}
       /[0-9]$/{print $1;next};
       /K(iB)?$/{printpower($1,  2, 10)};
       /M(iB)?$/{printpower($1,  2, 20)};
       /G(iB)?$/{printpower($1,  2, 30)};
       /T(iB)?$/{printpower($1,  2, 40)};
       /KB$/{    printpower($1, 10,  3)};
       /MB$/{    printpower($1, 10,  6)};
       /GB$/{    printpower($1, 10,  9)};
       /TB$/{    printpower($1, 10, 12)}'
  done
}

# ENV 
# CONFIG="max_allowed_packet=1G,innodb_buffer_pool_size=500M,max_connections=1000"
# WSREPCONFIG="wsrep_slave_threads=4"                   -- specific WSREP config items
# BINLOGCONFIG="expire-logs-days=1"                     -- specific Binary Log config items
# MYSQL_ROOT_PASSWORD=password                          -- use kube secret
# SSTUSER_PASSWORD=password                             -- use kube secret
# MONITOR_PASSWORD=password                             -- use kube secret
# CLUSTER_NAME=pxc_cluster                              -- wsrep cluster name - dont repeat in same namespace
# CLUSTER_JOIN=clientsrvname                            -- override service to join - if you allow unready pods to get DNS us the client service (ensure all ports are availiable)
# AUTOBOOTSTRAP="true"                                  -- automatically bootstraps node-0 (this is the default - disable for production, can cause loss of data on loss of quorum)
# BINLOG="true"                                         -- create a binlog
# SLAVE="true"                                          -- be a slave of another cluster
# REPLUSER="replication:password"                       -- replication credentials
# MASTER_HOST="pxc.srv.kubernetes"                      -- where your master is
# STARTFROMBACKUP="true"                                -- restore backup before starting
# BACKUPDIR="/var/master/backup"                        -- location of latest backup
# SSL = "client|all"                                    -- enable SSL - client: Just client traffic, all: client and wsrep, in CONFIG ssl-crt,ssl-key and ssl-ca required
# SCHEMA = "<schema to create if not exist>"            -- Create schema if not exist - must supply EXTUSER
# EXTUSER = "<username>:<password>"                     -- Create user with full rights on SCHEMA - neither user or password can contain : using simple split
# RLIMIT = "1m"                                         -- SST thottling setting (network limit) - dont set too high for your env
# SSTMEM = "200M"                                       -- SST apply mem limit
# AUDITINCLUDE = "root,user1"                           -- comma separated set of users to include for auditing
# PROXYMON = "proxymon:password"                        -- simple monitor user (Granted only USAGE)

OIFS=$IFS;
IFS=",";

CONFIGVARS=(${CONFIG})
WSREPVARS=(${WSREPCONFIG})
BINLOGVARS=(${BINLOGCONFIG})

if [ "$SSL" = "client" ] || [ "$SSL" = "all" ]; then
# Check if options have been supplied for SSL
SSLOPTS=$(echo $CONFIG | awk '/ssl-cert=/ && /ssl-ca=/ && /ssl-key=/' | wc -l)
if [ $SSLOPTS -ne 1 ]; then
echo "For SSL = 'client|all' must have ssl-cert,ssl-key and ssl-ca in extra config vars"
fi
fi

# Get service name of stateful set
if [ -z "$SSSVC" ]; then
  SSSVC=$(hostname -f | cut -s -d"." -f2)
fi
if [ -z "$CLUSTER_JOIN" ]; then
CLUSTER_JOIN=$SSSVC
fi
NODE_NAME=$(hostname)


#if [ "$(echo "$NODE_NAME" | awk -F- '{print $NF}')" = "0" ] && [ $CLUSTERUP -eq 0 ]; then
#Part of stateful set and DATADIR empty - zero out CLUSTER_JOIN else just start using persistant data
#export CLUSTER_JOIN=""
#fi

# Find peers and status if exist
NODEID=$(echo $(hostname) | rev | cut -d- -f1 | rev)
HOSTPREFIX=$(echo $(hostname) | rev | cut -d- -f2- | rev)
CLUSTERUP=0
if [ -z "$CLUSTERSIZE" ]; then
N=2
else
N=$((CLUSTERSIZE-1))
fi
echo "Looking for peers"
until [ $N -lt 0 ]; do
  if [ $N -ne $NODEID ]; then
     echo -n "Trying ${HOSTPREFIX}-${N}.${SSSVC}.."
        if [ $(getent hosts ${HOSTPREFIX}-${N}.${SSSVC} | wc -l) -eq 1 ]; then
           if $(nc -w 2 -z ${HOSTPREFIX}-${N}.${SSSVC} 3306 > /dev/null 2>&1) ; then
              echo -n "  [UP]"
              CLUSTERUP=1
           else
              echo -n "  [DOWN]"
           fi
        else
           echo -n "  [NOT KNOWN]"
        fi
     echo ""
  fi
N=$((N-1))
done 



mkdir -p /etc/my.cnf.d

if [ -n "$AUDITINCLUDE" ]; then
AUDITPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

cat > /etc/my.cnf.d/audit.cnf << EOF
[mysqld]
early-plugin-load="server_audit=server_audit.so"
early-plugin-load="connection_control=connection_control.so"
server_audit_events                             = QUERY,TABLE
server_audit_file_path                          = server_audit.log
server_audit_file_rotate_size                   = 500000
server_audit_file_rotations                     = 2
server_audit_incl_users                         = "${AUDITINCLUDE}"
server_audit_logging                            = ON
server_audit_mode                               = 0
server_audit_output_type                        = file
server_audit_query_log_limit                    = 1024
connection_control_failed_connections_threshold = 3
connection_control_max_connection_delay         = 2147483647
connection_control_min_connection_delay         = 1000
EOF

mkdir -p /var/lib/sql/
cat > /etc/my.cnf.d/init_audit.sql << EOA
ALTER DEFINER='audit'@'localhost' EVENT audit.audit_update ENABLE;
EOA

cat > /var/lib/sql/audit.sql << EOB
CREATE SCHEMA IF NOT EXISTS audit;
USE audit;
CREATE TABLE IF NOT EXISTS audit.server_audit_incl_users (
  User char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (User)
) ENGINE=InnoDB;
SET @r=0;
INSERT IGNORE INTO server_audit_incl_users (User)
SELECT
SUBSTRING_INDEX( SUBSTRING_INDEX(val, ',', id), ',', -1) AS include
FROM (
SELECT @r := @r + 1 AS id FROM mysql.user
) cnt
CROSS JOIN (SELECT CONCAT(REPLACE('${AUDITINCLUDE}','"',''),',') AS val) AS c
HAVING include <> '';
DELIMITER //
CREATE PROCEDURE audit.audit_check ()
BEGIN
DECLARE included VARCHAR(2048);
SET group_concat_max_len = 2048;
SET included=(SELECT GROUP_CONCAT(User ORDER BY User) FROM audit.server_audit_incl_users);
IF (SELECT @@server_audit_incl_users) <> included THEN
SET GLOBAL server_audit_incl_users=included;
END IF;
END //
DELIMITER ;
CREATE USER IF NOT EXISTS 'audit'@'localhost';
ALTER USER 'audit'@'localhost' IDENTIFIED BY '${AUDITPASS}';
GRANT EXECUTE,EVENT,SUPER,SELECT ON *.* TO 'audit'@'localhost';
SET GLOBAL server_audit_incl_users=(SELECT GROUP_CONCAT(User ORDER BY User) FROM audit.server_audit_incl_users);
DROP EVENT IF EXISTS audit_update;
CREATE DEFINER='audit'@'localhost' EVENT audit_update ON SCHEDULE EVERY 1 SECOND DO CALL audit.audit_check;
ALTER DEFINER='audit'@'localhost' EVENT audit_update ENABLE;
EOB
chmod -R +r /var/lib/sql
CONFIGVARS+=('init_file=/etc/my.cnf.d/init_audit.sql')
CONFIGVARS+=('event_scheduler=ON')
fi

# Basic my.cnf, defaults used here in bytes, dont remove otherwise autotune wont work
cat > /etc/my.cnf << EOF
[mysqld]
skip-character-set-client-handshake
collation_server                       = 'utf8mb4_unicode_ci'
character_set_server                   = 'utf8mb4'
character_set_filesystem               = 'utf8mb4'
!includedir     /etc/my.cnf.d/

disable-partition-engine-check
skip_name_resolve
explicit_defaults_for_timestamp        = 1
datadir                                = /var/lib/mysql
socket                                 = /var/lib/mysql/mysql.sock
pid-file                               = /var/run/mysqld/mysqld.pid
long_query_time                        = 0.5
slow_query_log                         = ON
slow_query_log_file                    = /var/lib/mysql/mysqld_slow.log
log_error_verbosity                    = 2
innodb_flush_method                    = O_DIRECT
join_buffer_size                       = 262144
sort_buffer_size                       = 262144
key_buffer_size                        = 8
myisam_sort_buffer_size                = 4096
read_buffer_size                       = 131072
read_rnd_buffer_size                   = 262144
innodb_log_buffer_size                 = 16777216
innodb_buffer_pool_size                = 134217728
innodb_buffer_pool_chunk_size          = 134217728
innodb_page_size                       = 16384
innodb_buffer_pool_instances           = 1
innodb_buffer_pool_dump_pct            = 100
innodb_buffer_pool_dump_at_shutdown    = ON
innodb_buffer_pool_load_at_startup     = ON
max_connections                        = 101
wait_timeout                           = 300
interactive_timeout                    = 300
table_definition_cache                 = 6000
table_open_cache                       = 6000
innodb_flush_log_at_trx_commit         = 0
symbolic-links                         = OFF
net_buffer_length                      = 16384
EOF

for ((i=0; i<${#CONFIGVARS[*]}; i++));
do
CONFIGNAME=$(echo ${CONFIGVARS[i]} | cut -d= -f1)
sed -i "/^${CONFIGNAME}/d" /etc/my.cnf
OPT=$(echo ${CONFIGVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${CONFIGVARS[i]} | cut -d= -f2 | sed "s/[[:space:]]//g")
if [[ "$VAL" =~ ^[0-9].* ]]; then
VAL=$( getbytes $VAL )
fi
echo "$(printf '%-36s %s ' ${OPT}) = $VAL" >> /etc/my.cnf
done

# Tune WSREP config based on CPU_LIMIT
if [ -n "$CPU_LIMIT" ]; then
  WSREPAPPLIERS=$((CPU_LIMIT*4))
else
  WSREPAPPLIERS=4
fi

cat > /etc/my.cnf.d/wsrep.cnf << EOG
[mysqld]
#WSREP Stuff
pxc_strict_mode                       = ENFORCING
wsrep_provider                        = /usr/lib64/galera3/libgalera_smm.so
wsrep_cluster_address                 = gcomm://${CLUSTER_JOIN}
wsrep_node_address                    = $(hostname -i)
wsrep_sst_receive_address             = $(hostname -i)
wsrep_log_conflicts                   = ON
wsrep_retry_autocommit                = 3
wsrep_cluster_name                    = ${CLUSTER_NAME}
wsrep_node_name                       = ${NODE_NAME}
wsrep_sst_method                      = xtrabackup-v2
wsrep_sst_auth                        = "sstuser:${SSTUSER_PASSWORD}"
wsrep_slave_threads                   = ${WSREPAPPLIERS}
wsrep_provider_options                = 'gcache.size=128M;gcache.recover=yes'
EOG

if [ -n "${BACKUPDIR}" ]; then
cat > /etc/my.cnf.d/file.cnf << EOJ
[mysqld]
secure_file_priv                      =${BACKUPDIR}
EOJ
fi

for ((i=0; i<${#WSREPVARS[*]}; i++));
do
CONFIGNAME=$(echo ${WSREPVARS[i]} | cut -d= -f1)
sed -i "/^${CONFIGNAME}/d" /etc/my.cnf.d/wsrep.cnf
OPT=$(echo ${WSREPVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${WSREPVARS[i]} | cut -d= -f2-50 | sed "s/[[:space:]]//g")
echo "$(printf '%-36s %s ' ${OPT}) = $VAL" >> /etc/my.cnf.d/wsrep.cnf
done

# XtraBackup config - none blocking SST agent
cat > /etc/my.cnf.d/xtrabackup.cnf << EOI
[xtrabackup]
compressor                           = 'pigz'
decompressor                         = 'pigz -dc'
inno-backup-opts                     = '--no-backup-locks'
EOI

if [ -z "$RLIMIT" ]; then
RLIMIT="5m"
fi
if [ -z "$SSTMEM" ]; then
SSTMEM="200M"
fi
cat > /etc/my.cnf.d/limitsst.cnf << EOK
[sst]
rlimit                               = $RLIMIT
inno-apply-opts                      = "--use-memory=$SSTMEM"
EOK

if [ "$SSL" = "all" ]; then
# Add SSL options to /etc/my.cnf.d/wsrep.cnf and create sst.cnf
cat > /etc/my.cnf.d/sst.cnf << EOG
[sst]
encrypt                              = 4
EOG

WSREPPROVIDER=()
for ((i=0; i<${#WSREPVARS[*]}; i++));
do
CONFIGNAME=$(echo ${WSREPVARS[i]} | cut -d= -f1)
OPT=$(echo ${WSREPVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${WSREPVARS[i]} | cut -d= -f2-50 | sed "s/[[:space:]]//g")
if [ $OPT = "wsrep_provider_options" ]; then
CURRENTCONF=$(echo $VAL | sed "s/'//g")
WSREPPROVIDER+=("${CURRENTCONF}")
fi
done

for ((i=0; i<${#CONFIGVARS[*]}; i++));
do
CONFIGNAME=$(echo ${CONFIGVARS[i]} | cut -d= -f1)
OPT=$(echo ${CONFIGVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${CONFIGVARS[i]} | cut -d= -f2 | sed "s/[[:space:]]//g")
if [[ "$OPT" =~ ssl-.* ]]; then
echo "$(printf '%-36s %s ' ${OPT}) = $VAL" >> /etc/my.cnf.d/sst.cnf
OPT=$(echo $OPT | tr "-" "_")
WSREPPROVIDER+=("socket.${OPT}=${VAL}")
fi
done

WSREPPROVIDEROUT=$(IFS=\; ; echo "${WSREPPROVIDER[*]}")
sed -i "/^wsrep_provider_options/d" /etc/my.cnf.d/wsrep.cnf
echo "$(printf '%-30s %s ' wsrep_provider_options) = '$WSREPPROVIDEROUT'" >> /etc/my.cnf.d/wsrep.cnf

fi


if [ "$BINLOG" = "true" ]; then
SERVERID=$(ip a | grep "inet.*eth0" | awk '{print $2}' | cut -d/ -f1 | tr -d .)
cat > /etc/my.cnf.d/binlog.cnf << EOH
[mysqld]
log-bin                                = mysql-bin.log
log-bin-index                          = mysql-bin.log.index
relay-log                              = mysql-relay.bin
relay-log-index                        = mysql-relay.bin.index
log-slave-updates                      = ON
log-slow-slave-statements              = ON
expire-logs-days                       = 3
max-binlog-size                        = 200M
binlog-format                          = ROW
gtid-mode                              = ON
enforce-gtid-consistency               = ON
server_id                              = ${SERVERID}
EOH
fi

for ((i=0; i<${#BINLOGVARS[*]}; i++));
do
CONFIGNAME=$(echo ${BINLOGVARS[i]} | cut -d= -f1)
sed -i "/^${CONFIGNAME}/d" /etc/my.cnf.d/binlog.cnf
OPT=$(echo ${BINLOGVARS[i]} | cut -d= -f1 | sed "s/[[:space:]]//g")
VAL=$(echo ${BINLOGVARS[i]} | cut -d= -f2-50 | sed "s/[[:space:]]//g")
echo "$(printf '%-36s %s ' ${OPT}) = $VAL" >> /etc/my.cnf.d/binlog.cnf
done

# Auto Tune
if [ -n "$AUTOTUNE" ] && [ -n "$MEM_LIMIT" ]; then


if [ "$AUTOTUNE" = "connections" ] || [ "$AUTOTUNE" = "bufferpool" ]; then
echo "Autotune set for $AUTOTUNE"
echo "collecting variables already set in /etc/my.cnf"
# grab variables (after they have been parsed fom CONFIG)
while IFS='' read -r line || [[ -n "$line" ]]; do
if [ ! "$line" = "" ]; then
line=$(echo "$line" | sed "s/[[:space:]]//g")
OPT=${line%=*}  # retain the part before the =
VAL=${line##*=}  # retain the part after the =
if [[ "$VAL" =~ ^[0-9]+$ ]]; then
eval "$OPT"=$VAL
fi
fi
done < /etc/my.cnf
# Give ourselves a 10% buffer 
MEM_LIMIT=$(( (MEM_LIMIT/100) * 90 ))
echo "Giving a 10% buffer so memory limit is $MEM_LIMIT"
# connection mem, docs say each connection uses at leat 2Mb + extras
USERMEM=$((sort_buffer_size + myisam_sort_buffer_size + read_buffer_size + read_rnd_buffer_size + 2097152))
echo "each connection will use $USERMEM bytes"
# Total connection memory
TOTUSERMEM=$(( USERMEM * max_connections ))
MEM=0

# Tune for max connections
if [ $AUTOTUNE = "connections" ]; then
echo "tuning for max number of connections"
until [ $MEM -gt $((MEM_LIMIT-USERMEM)) ]; do
TOTUSERMEM=$(( USERMEM * max_connections ))
TOTSYSMEM=$(( key_buffer_size + innodb_buffer_pool_size + innodb_log_buffer_size + net_buffer_length ))
MEM=$(( TOTUSERMEM + TOTSYSMEM ))
max_connections=$((max_connections+1))
done
echo "connections set at $max_connections"
fi

# Tune for max bufferpool
if [ $AUTOTUNE = "bufferpool" ]; then
echo "tuning for bufferpool"
until [ $MEM -gt $((MEM_LIMIT-innodb_buffer_pool_chunk_size)) ]; do
TOTSYSMEM=$(( key_buffer_size + innodb_buffer_pool_size + innodb_log_buffer_size + net_buffer_length ))
MEM=$((TOTSYSMEM+TOTUSERMEM))
innodb_buffer_pool_size=$((innodb_buffer_pool_size+innodb_buffer_pool_chunk_size))
done
echo "bufferpool size set at $innodb_buffer_pool_size bytes"
fi
# add the tuned values back into config before start
echo "adding tuned values to /etc/my.cnf"
sed -i "/^max_connections.*$/d" /etc/my.cnf
sed -i "/^innodb_buffer_pool_size.*$/d" /etc/my.cnf
echo "$(printf '%-36s %s ' innodb_buffer_pool_size) = $innodb_buffer_pool_size" >> /etc/my.cnf
echo "$(printf '%-36s %s ' max_connections) = $max_connections" >> /etc/my.cnf

# If memlimit not passed in or buffer pool less than 1G then use 1 instance
#POOLSIZE=$(cat /etc/my.cnf | grep innodb_log_buffer_size | cut -d= -f2 | sed "s/[[:space:]]//g")
echo "calculating correct number of bufferpools for $innodb_buffer_pool_size"
if [ -z "$MEM_LIMIT" ] || [ $innodb_buffer_pool_size -lt 1073741824 ]; then
  sed -i "/^innodb_buffer_pool_instances.*$/d" /etc/my.cnf
  BUFFPOOLS=1
fi

# If memlimit passed in and buffer pool > 1G - buffer pools = floor of buffer pool size / 1G
if [ -n "$MEM_LIMIT" ] && [ $innodb_buffer_pool_size -gt 1073741824 ]; then
BUFFPOOLS=$((innodb_buffer_pool_size / 1073741824))
 if [ -n "$CPU_LIMIT" ] && [ $BUFFPOOLS -gt $CPU_LIMIT ]; then
  BUFFPOOLS=$CPU_LIMIT
 fi
fi
sed -i "/^innodb_buffer_pool_instances.*$/d" /etc/my.cnf
echo "$(printf '%-36s %s ' innodb_buffer_pool_instances) = ${BUFFPOOLS}" >> /etc/my.cnf
echo "bufferpools set at ${BUFFPOOLS}"
fi
fi


chmod 0755 /etc/my.cnf
chmod -R 0755 /etc/my.cnf.d
chown -R mysql:mysql /var/lib/mysql

rm -f /root/.my.cnf

IFS=$OIFS

INIT=-1
#0 = cluster down, first node, no data dir & no start from backup
#1 = cluster down, first node, no data dir & start from backup, backup dir exists
#2 = no data dir, cluster is up, try start and recieve SST 
#4 = data is present, attempt start
if [ $NODEID -eq 0 ] && [ ! "${STARTFROMBACKUP}" = "true" ] && [ ! -d "/var/lib/mysql/mysql" ] && [ $CLUSTERUP -eq 0 ]; then 
  INIT=0
elif [ $NODEID -eq 0 ] && [ "${STARTFROMBACKUP}" = "true" ] && [ ! -d "/var/lib/mysql/mysql" ] && [ -n "${BACKUPDIR}" ] && [ -d "${BACKUPDIR}" ] && [ $CLUSTERUP -eq 0 ]; then 
  INIT=1
elif [ $CLUSTERUP -eq 1 ] && [ ! -d "/var/lib/mysql/mysql" ]; then 
  INIT=2
elif [ -d "/var/lib/mysql/mysql" ]; then 
  INIT=3  
fi
if [ $INIT -eq -1 ]; then
echo "ENV variables not set correctly or cluster down and this is not node-0"
echo "need at least:"
echo "MYSQL_ROOT_PASSWORD=<password of all root users>"
echo "SSTUSER_PASSWORD=<password for SST user>"
echo "CLUSTER_NAME=<name of cluster>"
exit 1
fi

case $INIT in
0)
echo "This is the first node in a stateful set and it has no grant tables and no other nodes are up - initialize"
mv /etc/my.cnf.d/wsrep.cnf /etc/my.cnf.d/wsrep.cnfx
if [ -n "$AUDITINCLUDE" ]; then
mv /etc/my.cnf.d/audit.cnf /etc/my.cnf.d/audit.cnfx
sed -i "s/^init_file/#init_file/g" /etc/my.cnf
fi
rm -rf /var/lib/mysql/*
mysqld --initialize-insecure --datadir=/var/lib/mysql/ --user=mysql
chown -R mysql:mysql /var/lib/mysql
chmod -R 0755 /var/lib/mysql
mysqld --user=mysql --datadir=/var/lib/mysql/ --skip-networking --log-error=/var/lib/mysql/mysqld.log &
for i in {1..30}; do
  echo -n ".."
  if $(mysql -u root -Nse "SELECT 1;" > /dev/null 2>&1) ; then
    break
  fi
  sleep 2
done
if [ $i -eq 30 ]; then
  echo "ERROR: Failed to initialize"
  exit 1
fi
if $(/usr/bin/mysqladmin -u root password "${MYSQL_ROOT_PASSWORD}") ; then
cat > /root/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
EOF
echo "Setting root password"
else
echo "ERROR: Failed to set root password"
exit 1
fi
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'root'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION;"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'sstuser'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'sstuser'@'localhost' IDENTIFIED BY '${SSTUSER_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT RELOAD,PROCESS,LOCK TABLES,REPLICATION CLIENT ON *.* TO 'sstuser'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'127.0.0.1' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS,REPLICATION SLAVE ON *.* TO 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'localhost' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS,REPLICATION SLAVE ON *.* TO 'monitor'@'localhost';"
if [ -n "${SCHEMA}" ] && [ -n "${EXTUSER}" ]; then # must be supplied <username>:<passowrd> (neither user or password can contain :)
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE SCHEMA IF NOT EXISTS ${SCHEMA};"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '$(echo "${EXTUSER}" | cut -d: -f1)'@'%' IDENTIFIED BY '$(echo "${EXTUSER}" | cut -d: -f2)';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON ${SCHEMA}.* TO '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
fi
if [ -n "${REPLUSER}" ]; then
REPLUSR=$(echo ${REPLUSER} | cut -d: -f1)
REPLPAS=$(echo ${REPLUSER} | cut -d: -f2)
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '${REPLUSR}'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '${REPLUSR}'@'%' IDENTIFIED BY '${REPLPAS}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO '${REPLUSR}'@'%';"
fi
if [ -n "${PROXYMON}" ]; then
PROXYUSR=$(echo ${PROXYMON} | cut -d: -f1)
PROXYPAS=$(echo ${PROXYMON} | cut -d: -f2)
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '${PROXYUSR}'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '${PROXYUSR}'@'%' IDENTIFIED BY '${PROXYPAS}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT USAGE ON *.* TO '${PROXYUSR}'@'%';"
fi
if [ -n "$AUDITINCLUDE" ]; then
mysql -Nse "INSTALL PLUGIN server_audit SONAME 'server_audit.so';"
mysql -Nse "INSTALL PLUGIN connection_control SONAME 'connection_control.so';"
mysql -Nse "INSTALL PLUGIN connection_control_failed_login_attempts SONAME 'connection_control.so';"
mysql -Nse "SOURCE /var/lib/sql/audit.sql;"
rm -f /var/lib/sql/audit.sql
sed -i "s/^#init_file/init_file/g" /etc/my.cnf
fi
if [ $(ls /var/lib/sql/ | grep -c ".sql") -gt 0 ]; then
for f in `ls /var/lib/sql/*.sql`; do
echo "Adding SQL file $f"
  mysql -Nse "SOURCE $f;"
done
fi
killall mysqld
until [ $(pgrep -c mysqld) -eq 0 ]; do sleep 1; echo -n "."; done
echo ""
mv /etc/my.cnf.d/wsrep.cnfx /etc/my.cnf.d/wsrep.cnf
if [ -n "$AUDITINCLUDE" ]; then
mv /etc/my.cnf.d/audit.cnfx /etc/my.cnf.d/audit.cnf
fi
;;
1)
echo "This is the first node in a stateful set, it has no grant tables, no other cluster nodes are up and is set to start from backup"
# backups done using #>innobackupex --stream=tar ./ | gzip - > backup.tar.gz
chown -R mysql:mysql ${BACKUPDIR}
mv /etc/my.cnf.d/wsrep.cnf /etc/my.cnf.d/wsrep.cnfx
mv /etc/my.cnf.d/xtrabackup.cnf /etc/my.cnf.d/xtrabackup.cnfx 
if [ -n "$AUDITINCLUDE" ]; then
mv /etc/my.cnf.d/audit.cnf /etc/my.cnf.d/audit.cnfx
sed -i "s/^init_file/#init_file/g" /etc/my.cnf
fi
touch /tmp/live.lock
BACKUPFILE=$(ls ${BACKUPDIR}/ | grep ".tar.gz$" | sort | tail -n1)
pv -L ${RLIMIT} ${BACKUPDIR}/${BACKUPFILE} | zcat | tar -xiC /var/lib/mysql
innobackupex --apply-log /var/lib/mysql
chown -R mysql:mysql /var/lib/mysql
chmod -R 0755 /var/lib/mysql
rm -f /var/lib/mysql/auto.cnf
rm -f /var/log/mysql/xtrabackup_galera_info
rm -f /var/log/mysql/gvwstate.dat
rm -f /var/log/mysql/grastate.dat
mysqld_safe --user=mysql --datadir=/var/lib/mysql/ --skip-networking --skip-grant-tables --log-error=/var/lib/mysql/reset_root.log &
for i in {1..30}; do
  echo -n ".."
  if $(mysql -u root -Nse "SELECT 1;" > /dev/null 2>&1) ; then
    break
  fi
  sleep 2
done
if [ $i -eq 30 ]; then
  echo "ERROR: Failed to reset root password on backup"
  sleep 3600
  exit 1
fi
mysql -u root -Nse "RESET MASTER;FLUSH PRIVILEGES;UPDATE mysql.user SET authentication_string=PASSWORD('${MYSQL_ROOT_PASSWORD}') WHERE User='root';FLUSH PRIVILEGES;"
killall mysqld_safe mysqld
until [ $(pgrep -c mysqld) -eq 0 ]; do sleep 1; echo -n "."; done
echo ""
cat > /root/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
EOF
mysqld --user=mysql --datadir=/var/lib/mysql/ --skip-networking --log-error=/var/lib/mysql/mysqld.log &
for i in {1..30}; do
  echo -n ".."
  if $(mysql -Nse "SELECT 1;" > /dev/null 2>&1) ; then
    break
  fi
  sleep 2
done
if [ $i -eq 30 ]; then
  echo "ERROR: Failed to start to add users"
  exit 1
fi
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'root'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION;"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'sstuser'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'sstuser'@'localhost' IDENTIFIED BY '${SSTUSER_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT RELOAD,PROCESS,LOCK TABLES,REPLICATION CLIENT ON *.* TO 'sstuser'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'127.0.0.1' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS,REPLICATION SLAVE ON *.* TO 'monitor'@'127.0.0.1';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS 'monitor'@'localhost';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER 'monitor'@'localhost' IDENTIFIED BY '${MONITOR_PASSWORD}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE USER,REPLICATION CLIENT,SHOW DATABASES,SUPER,PROCESS,REPLICATION SLAVE ON *.* TO 'monitor'@'localhost';"
if [ -n "${SCHEMA}" ] && [ -n "${EXTUSER}" ]; then # must be supplied <username>:<passowrd> (neither user or password can contain :)
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT SELECT,CREATE SCHEMA IF NOT EXISTS ${SCHEMA};"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '$(echo "${EXTUSER}" | cut -d: -f1)'@'%' IDENTIFIED BY '$(echo "${EXTUSER}" | cut -d: -f2)';"
  mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT ALL ON ${SCHEMA}.* TO '$(echo "${EXTUSER}" | cut -d: -f1)'@'%';"
fi
if [ -n "$AUDITINCLUDE" ]; then
mysql -Nse "INSTALL PLUGIN server_audit SONAME 'server_audit.so';"
mysql -Nse "INSTALL PLUGIN connection_control SONAME 'connection_control.so';"
mysql -Nse "INSTALL PLUGIN connection_control_failed_login_attempts SONAME 'connection_control.so';"
mysql -Nse "SOURCE /var/lib/sql/audit.sql;"
rm -f /var/lib/sql/audit.sql
fi
if [ -n "${REPLUSER}" ]; then
REPLUSR=$(echo ${REPLUSER} | cut -d: -f1)
REPLPAS=$(echo ${REPLUSER} | cut -d: -f2)
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '${REPLUSR}'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '${REPLUSR}'@'%' IDENTIFIED BY '${REPLPAS}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO '${REPLUSR}'@'%';"
fi
if [ -n "${PROXYMON}" ]; then
PROXYUSR=$(echo ${PROXYMON} | cut -d: -f1)
PROXYPAS=$(echo ${PROXYMON} | cut -d: -f2)
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;CREATE USER IF NOT EXISTS '${PROXYUSR}'@'%';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;ALTER USER '${PROXYUSR}'@'%' IDENTIFIED BY '${PROXYPAS}';"
mysql -Nse "SET @@SESSION.SQL_LOG_BIN=0;GRANT USAGE ON *.* TO '${PROXYUSR}'@'%';"
fi
if [ "${SLAVE}" = "true" ] && [ -n "${REPLUSER}" ] && [ -n "${MASTER_HOST}" ]; then # can only happen if started fom backup
REPLUSR=$(echo ${REPLUSER} | cut -d: -f1)
REPLPAS=$(echo ${REPLUSER} | cut -d: -f2)
LASTGTID=$(cat /var/lib/mysql/xtrabackup_binlog_info  | tr -d "\n" | awk '{print $3}')
mysql -Nse "RESET SLAVE;"
mysql -Nse "SET GLOBAL GTID_PURGED=\"${LASTGTID}\";"
mysql -Nse "CHANGE MASTER TO MASTER_HOST=\"${MASTER_HOST}\", MASTER_USER=\"${REPLUSR}\", MASTER_PASSWORD=\"${REPLPAS}\", MASTER_PORT=3306, MASTER_AUTO_POSITION = 1;"
mysql -Nse "START SLAVE;"
mysql -Nse "SET GLOBAL read_only=ON;"
echo "read_only=ON" >> /etc/my.cnf
sed -i "s/^#init_file/init_file/g" /etc/my.cnf
sleep 5 # give it chance to connect
fi

if [ $(ls /var/lib/sql/ | grep -c ".sql") -gt 0 ]; then
for f in `ls /var/lib/sql/*.sql`; do
echo "Adding SQL file $f"
  mysql -Nse "SOURCE $f;"
done
fi

killall mysqld
until [ $(pgrep -c mysqld) -eq 0 ]; do sleep 1; echo -n "."; done
echo ""
rm /tmp/live.lock
mv /etc/my.cnf.d/wsrep.cnfx /etc/my.cnf.d/wsrep.cnf
mv /etc/my.cnf.d/xtrabackup.cnfx /etc/my.cnf.d/xtrabackup.cnf
if [ -n "$AUDITINCLUDE" ]; then
mv /etc/my.cnf.d/audit.cnfx /etc/my.cnf.d/audit.cnf
fi
;;
2)
echo "This is not the first node in a stateful set and has no grant tables and at least one node is up - SST"
rm -rf /var/lib/mysql/*
chown -R mysql:mysql /var/lib/mysql
chmod -R 755 /var/lib/mysql
cat > /root/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
EOF
;;
3)
echo "Grant tables are present just attempt a start"
chown -R mysql:mysql /var/lib/mysql
chmod -R 755 /var/lib/mysql
cat > /root/.my.cnf << EOF
[client]
user     = root
password = "${MYSQL_ROOT_PASSWORD}"
EOF
;;
esac;


if [ -n "${BACKUPDIR}" ]; then
# backup script
cat > /xtrabackup.sh << EOK
#!/bin/bash
if [ ! -f /var/lib/mysql/backup.lock ]; then
touch /var/lib/mysql/backup.lock
innobackupex --stream=tar ./ | gzip - > ${BACKUPDIR}/backup-${CLUSTER_NAME}-\$(date +%s).tar.gz
rm /var/lib/mysql/backup.lock
fi
EOK
chmod +x /xtrabackup.sh

cat > /xtrabackup-inc.sh << EOL
#!/bin/bash
# Incremental based on full backup at midnight (24 backups per day 1 big 23 approx same size)
#
# To prepare to a certain hour e.g. 12:00:
#  Prepare each hour in turn:
#   xtrabackup --prepare --apply-log-only --target-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD00 --incremental-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD01
#   xtrabackup --prepare --apply-log-only --target-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD00 --incremental-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD02
#   ....
#   xtrabackup --prepare --apply-log-only --target-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD00 --incremental-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD12
# Then restore /var/backup/<cluster>/YYYYMMDD/YYYYMMDD00 as normal, on start crash recovery will take care of rollback phase

mkdir -p ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")

if [ \$(date +"%H") -eq 0 ] || [ ! -d ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d00") ]; then
xtrabackup --throttle=5 --backup --target-dir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d00") --datadir=/var/lib/mysql/
else
LAST=\$(ls -la ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d") | awk '{print \$9}' | sort | tail -n1)
xtrabackup --throttle=5 --backup --target-dir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d%H") --incremental-basedir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\${LAST} --datadir=/var/lib/mysql/
fi
EOL
chmod +x /xtrabackup-inc.sh

cat > /xtrabackup-diff.sh << EOM
#!/bin/bash
# Differential based on full backup at midnight (24 backups per day 1 big 23 increasing in size)
#
# To prepare to a certain hour e.g. 12:00:
#  Prepare just that hour:
#   xtrabackup --prepare --apply-log-only --target-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD00 --incremental-dir=/var/backup/<cluster>/YYYYMMDD/YYYYMMDD12
# Then restore /var/backup/<cluster>/YYYYMMDD/YYYYMMDD00 as normal, on start crash recovery will take care of rollback phase

mkdir -p ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")

if [ \$(date +"%H") -eq 0 ] || [ ! -d ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d00") ]; then
xtrabackup --throttle=5 --backup --target-dir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d00") --datadir=/var/lib/mysql/
else
xtrabackup --throttle=5 --backup --target-dir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d00") --incremental-basedir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d%H") --datadir=/var/lib/mysql/
fi
EOM
chmod +x /xtrabackup-diff.sh

cat > /xtrabackup-weekly-hourly-diff.sh << EON
#!/bin/bash
# Differential based on full backup at midnight Sunday and hourly differential backups until the following Sunday
#
mkdir -p ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +%U)/\$(date +"%Y%m%d")

if [ -d ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +%U)/\$(date +"%Y%m%d")/\$(date +"%Y%m%d%H") ]; then
  echo "Backup directory present - exiting"
  exit 0
fi

if [ \$(date +"%u") -eq 7 ]; then
# This is Sunday do a full backup - will go in /backupdir/weekofyear(starting Sunday)/YYYYMMDD/YYYYMMDD00

mkdir -p ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +%U)/\$(date +"%Y%m%d")
xtrabackup --throttle=5 \
--backup --innodb_use_native_aio=0 \
--target-dir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +%U)/\$(date +"%Y%m%d")/\$(date +"%Y%m%d%H") \
--datadir=/var/lib/mysql

else 
# Do differential based on last Sunday - will go in /nfs/mountpath/weekofyear(starting Sunday)/YYYYMMDD/YYYYMMDDHH

if [ -d ${BACKUPDIR}/${CLUSTER_NAME}/\$(date +%U -d "last Sunday")/\$(date +"%Y%m%d" -d "last Sunday")/\$(date +"%Y%m%d00" -d "last Sunday") ]; then

xtrabackup --throttle=5 \
--backup --innodb_use_native_aio=0 \
--target-dir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d")/\$(date +"%Y%m%d%H") \
--incremental-basedir=${BACKUPDIR}/${CLUSTER_NAME}/\$(date +"%Y%m%d" -d "last Sunday")/\$(date +"%Y%m%d00" -d "last Sunday") \
--datadir=/var/lib/mysql

else

echo "Full backup not present"
exit 1

fi
EON
chmod +x /xtrabackup-weekly-hourly-diff.sh
fi

if [ -z "$AUTOBOOTSTRAP" ]; then
BOOTSTRAP=1
elif [ "$AUTOBOOTSTRAP" = "true" ]; then
BOOTSTRAP=1
else
BOOTSTRAP=0
fi
if [ $CLUSTERUP -eq 0 ] && [ $NODEID -eq 0 ] && [ $BOOTSTRAP -eq 1 ]; then # stop them all from bootstraping (docker has issues knowing cluster state)
  if [ -f /var/lib/mysql/grastate.dat ]; then
  sed -i "s/^safe_to_bootstrap.*$/safe_to_bootstrap: 1/g" /var/lib/mysql/grastate.dat
  chown mysql:mysql /var/lib/mysql/grastate.dat
  fi
  BOOTARGS="--wsrep-new-cluster"
fi
if [ -f /var/lib/sql/audit.sql ]; then
rm -f /var/lib/sql/audit.sql
fi

pid=0
# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    # Draining persistant connections - check if we can connect - if not just exit as all threads will have exited anyway
    touch /tmp/drain.lock # add lock file to make clustercheck.sh exit 1
    if [ $(mysql --defaults-file=/root/.my.cnf -Nse "SELECT 1;" 2>/dev/null) ]; then
    CONNS=$(mysql --defaults-file=/root/.my.cnf -Nse "SELECT COUNT(*) FROM information_schema.PROCESSLIST WHERE User NOT IN ('root','system user','sstuser','monitor');" 2>/dev/null )
    START=$(date +%s)
    NOW=$(date +%s)
    echo ""
    echo "Connected users:"
      while [ ${CONNS} -gt 0 ]; do
        for con in $(mysql --defaults-file=/root/.my.cnf -Nse "SELECT ID FROM information_schema.PROCESSLIST WHERE User NOT IN ('root','system user','sstuser','monitor');" 2>/dev/null ); do
        if [ "$(mysql --defaults-file=/root/.my.cnf -Nse "SELECT IF(COMMAND='Sleep',1,0) FROM information_schema.PROCESSLIST WHERE ID=$con;" 2>/dev/null )" = "1" ]; then
        mysql --defaults-file=/root/.my.cnf -Nse "KILL CONNECTION $con;" 2>/dev/null
        fi
        done
      CONNS=$(mysql --defaults-file=/root/.my.cnf -Nse "SELECT COUNT(*) FROM information_schema.PROCESSLIST WHERE User NOT IN ('root','system user','sstuser','monitor');" 2>/dev/null )
      echo -en "..${CONNS}     \r"
      # only try for 60 seconds
      NOW=$(date +%s)
      if [ $NOW -gt $((START+60)) ]; then
      break
      fi
      done
    fi
    # given it 60 seconds to clear, now force all connections off
    mysql -Nse "SET GLOBAL wsrep_reject_queries=ALL_KILL;"
    kill "$pid"
    wait "$pid"
  fi
  exit 0;
}

# setup handlers
# on callback, kill the last background process, which is `tail -f /dev/null` and execute the specified handler
trap 'kill ${!}; term_handler' SIGKILL SIGTERM SIGHUP SIGINT EXIT

# run application
echo "Starting MySQL"
eval "mysqld --datadir=/var/lib/mysql/ --user=mysql $BOOTARGS 2>&1 &"
pid="$!"

# wait indefinitely
while true
do 
tail -f /dev/null & wait ${!}
done
