#!/bin/bash 
#

if [[ $1 == '-h' || $1 == '--help' ]];then
    echo "Usage: $0 <user> <pass> <available_when_donor=0|1> <log_file> <available_when_readonly=0|1> <defaults_extra_file>"
    exit
fi

if [ -f /tmp/drain.lock ]; then
echo "Node is draining"
exit 1
fi

MYSQL_USERNAME="${1-monitor}" 
MYSQL_PASSWORD="${2-${MONITOR_PASSWORD}}" 
AVAILABLE_WHEN_DONOR=${3:-1}
ERR_FILE="${4:-/var/lib/mysql/clustercheck.log}" 
AVAILABLE_WHEN_READONLY=${5:-1}
DEFAULTS_EXTRA_FILE=${6:-/etc/my.cnf}

#Timeout exists for instances where mysqld may be hung
TIMEOUT=10

EXTRA_ARGS="--protocol=TCP --host=127.0.0.1"
if [[ -n "$MYSQL_USERNAME" ]]; then
    EXTRA_ARGS="$EXTRA_ARGS --user=${MYSQL_USERNAME}"
fi
if [[ -n "$MYSQL_PASSWORD" ]]; then
    EXTRA_ARGS="$EXTRA_ARGS --password=${MYSQL_PASSWORD}"
fi
if [[ -r $DEFAULTS_EXTRA_FILE ]];then 
    MYSQL_CMDLINE="mysql --defaults-extra-file=$DEFAULTS_EXTRA_FILE -nNE --connect-timeout=$TIMEOUT ${EXTRA_ARGS}"
else 
    MYSQL_CMDLINE="mysql -nNE --connect-timeout=$TIMEOUT ${EXTRA_ARGS}"
fi

ipaddr=$(hostname -i | awk ' { print $1 } ')
hostname=$(hostname)

if [ $(ps aux | grep -c [-]-wsrep-new-cluster) -eq 1 ]; then
STATE=$(mysql --defaults-file=/root/.my.cnf -Nse "SHOW STATUS LIKE 'wsrep_%';" 2>${ERR_FILE} | egrep 'wsrep_local_state[[:space:]]|wsrep_cluster_status|wsrep_cluster_size' | sort | awk '{print $2}')
STATE=$(echo $STATE | sed "s/[[:space:]]/:/g")

if [ $(echo $STATE | cut -d: -f1) -ge 3 ] && [ $(echo $STATE | cut -d: -f2) = "Primary" ] && [ $(echo $STATE | cut -d: -f3) -eq 4 ]; then
echo "Un Bootstrapping"
killall mysqld
exit 1
fi

fi

if [ ! $(mysql --defaults-file=/root/.my.cnf -se "show status like 'wsrep_cluster_status'\G" 2>${ERR_FILE} | grep Value | awk '{print $2}') = "Primary" ] && [ $(mysql --defaults-file=/root/.my.cnf -se "SHOW STATUS LIKE 'wsrep_cluster_size'\G" 2>${ERR_FILE} | grep Value | awk '{print $2}') -eq 1 ]; then
# Cluster has lost quoram and is down to one node, try and recover
mysql --defaults-file=/root/.my.cnf -Nse "SET GLOBAL wsrep_provider_options='pc.bootstrap=TRUE'"
fi

#
# Perform the query to check the wsrep_local_state
#
WSREP_STATUS=($($MYSQL_CMDLINE -e "SHOW GLOBAL STATUS LIKE 'wsrep_%';" 2>${ERR_FILE} | grep -A 1 -E 'wsrep_local_state$|wsrep_cluster_status$' | sed -n -e '2p'  -e '5p' | tr '\n' ' '))
 
if [[ ${WSREP_STATUS[1]} == 'Primary' && ( ${WSREP_STATUS[0]} -eq 4 || ( ${WSREP_STATUS[0]} -eq 2 && $AVAILABLE_WHEN_DONOR -eq 1 ) ) ]]
then 

    # Check only when set to 0 to avoid latency in response.
    if [[ $AVAILABLE_WHEN_READONLY -eq 0 ]];then
        READ_ONLY=$($MYSQL_CMDLINE -e "SHOW GLOBAL VARIABLES LIKE 'read_only';" \
                    2>${ERR_FILE} | tail -1 2>>${ERR_FILE})

        if [[ "${READ_ONLY}" == "ON" ]];then 
            # Percona XtraDB Cluster node local state is 'Synced', but it is in
            # read-only mode. The variable AVAILABLE_WHEN_READONLY is set to 0.
            # Shell return-code is 1
	    exit 1
        fi

    fi
    # Percona XtraDB Cluster node local state is 'Synced'
    # Shell return-code is 0
    exit 0
else 
    # Percona XtraDB Cluster node local state is NOT 'Synced'
    # Shell return-code is 1
    exit 1
fi 