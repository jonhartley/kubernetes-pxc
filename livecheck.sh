#!/bin/bash

# Liveness check just looks for a running mysqld process, this stops the container being restarted during large SST
# added the abiliy to overide by touching a lock file, during recovery of data ensure pod does not restart

if [ $(pgrep mysqld) ] || [ -f "/tmp/live.lock" ]; then
exit 0
else
exit 1
fi